//
//  TextViewController.swift
//  PageViewControllerDemo
//
//  Created by mohammad.sheikh on 08/04/22.
//

import UIKit

class TextViewController: UIViewController {

    var myText: String
    
    private let myTextView: UITextView = {
        let textView = UITextView()
        textView.isEditable = false
        textView.font = .systemFont(ofSize: 24)
        textView.textColor = .black
        textView.backgroundColor = .green
        return textView
    }()
    
    init(with text: String){
        self.myText = text
        myTextView.text = self.myText
        super.init(nibName: nil, bundle: nil)
       
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.addSubview(self.myTextView)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        myTextView.frame = view.bounds
        
    }

}
